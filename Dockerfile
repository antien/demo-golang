FROM golang:latest

ENV HOST 192.168.1.8

ENV WDIR $GOPATH/src/test

RUN mkdir -p $GOPATH/src/test

ADD . $WDIR

WORKDIR $WDIR

RUN apt-get install git

RUN ls && go build -o test .


CMD ["./test"]

EXPOSE 8089
